import React, { Component } from 'react';
import { AtomicBlockUtils, Editor, EditorState, convertToRaw } from 'draft-js';
import ControlButtons from '../RichTextEditor/ControlButtons';

import './index.css';

class MediaEditor extends Component {
  constructor (props) {
    super(props)
    this.state = {
      editorState: EditorState.createEmpty(),
      showURLInput: false,
      urlValue: '',
      urlType: '',
    };
  }

  // focus on editor
  focus = () => this.refs.editor.focus();

  // this function allow us to save and display changes while user is writing
  onChange = (editorState) => this.setState({editorState});
  
  // add url to state
  onURLChange = (event) => this.setState({urlValue: event.target.value});

  addMedia = (type) => {
    this.setState({
      showURLInput: true,
      urlValue: '',
      urlType: type,
    });
  }

  confirmMedia = (event) => {
    event.preventDefault();
    const {editorState, urlValue, urlType} = this.state;
    const contentState = editorState.getCurrentContent();
    /* 
    createEntity() - returns ContentState record updated to 
    include the newly created DraftEntity record in it's EntityMap. 
    */
    const contentStateWithEntity = contentState.createEntity(
      urlType,
      'IMMUTABLE',
      {src: urlValue}
    );
    console.log('contentStateWithEntity');
    console.log(convertToRaw(contentStateWithEntity));

    // getLastCreatedEntityKey() return the key of the newly created DraftEntity record 
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(
      editorState,
      {currentContent: contentStateWithEntity}
    );

    this.setState({
      editorState: AtomicBlockUtils.insertAtomicBlock(
        newEditorState,
        entityKey,
        ' '
      ),
      showURLInput: false,
      urlValue: '',
    }, () => {
      setTimeout(() => this.focus(), 0);
    });
  }

  render() {
    return (
      <div className = "editor-root">
        <MediaControls 
          editorState = {this.state.editorState}
          onToggle = {this.addMedia}
          showURLInput = {this.state.showURLInput}
          confirmMedia = {this.confirmMedia}
          onURLChange = {this.onURLChange}
        />
        <div
          className = "editor"
          onClick = {this.focus}
        >
          <Editor
          blockRendererFn={mediaBlockRenderer}
          editorState={this.state.editorState}
          onChange = {this.onChange}
          placeholder="Image editor"
          ref="editor"
          />
        </div>
      </div>
    );
  }
}

export default MediaEditor;

var MEDIA = [
  {label: 'Image', type: 'image'},
  {label: 'Audio', type: 'audio'},
  {label: 'Video', type: 'video'},
];

const MediaControls = (props) => {
  let urlInput;
  
  if (props.showURLInput) {
    urlInput =
    <div>
      <input
        onChange={props.onURLChange}
        type="text"
      />
      <button onMouseDown={props.confirmMedia}>
        Confirm
      </button>
    </div>;
  }

  return (
    <div className = "controls">
      {MEDIA.map((type) => 
        <ControlButtons 
          key = {type.label}
          label = {type.label}
          onToggle = {props.onToggle.bind(null, type.type)}
        />  
      )}
      {urlInput}
    </div>
  )
}

function mediaBlockRenderer(block) {
  if (block.getType() === 'atomic') {
    return {
      component: Media,
      /* 
      optional editable property determines whether the custom component is contentEditable
      recommended that you use editable: false if your custom component will not contain text
      */
      editable: false,
    };
  }
  return null;
}

const Audio = (props) => {
  return <audio controls src={props.src} />;
};

const Image = (props) => {
  return <img src={props.src} />;
};

const Video = (props) => {
  return <video controls src={props.src} />;
};

const Media = (props) => {
  const entity = props.contentState.getEntity(
    props.block.getEntityAt(0)
  );
  const {src} = entity.getData();
  const type = entity.getType();

  let media;
  switch (type) {
    case 'audio':
      media = <Audio src={src} />;
      break;
    
    case 'image':
      media = <Image src={src} />;
      break;

    case 'video':
      media = <Video src={src} />;
      break;
  }

  return media;
};