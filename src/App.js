import React, { Component } from 'react';
import RichTextEditor from './RichTextEditor/index';
import MediaEditor from './MediaEditor/index';
import ImageEditor from './DraftjsImagePlugin/index'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="editors">
        <RichTextEditor />
        <MediaEditor />
        <ImageEditor />
      </div>
    );
  }
}

export default App;
