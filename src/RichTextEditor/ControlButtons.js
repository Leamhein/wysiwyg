import React, { Component } from 'react';

class ControlButtons extends Component {

  onToggle = (event) => {
    event.preventDefault();
    this.props.onToggle(this.props.style)
  }

  render() {
    let className = 'controlButton';
    if (this.props.active) {
      className += ' activeButton';
    }

    return (
      <span
        className = {className}
        onMouseDown = {this.onToggle}
      >
        {this.props.label}
      </span>
    );
  }
}

export default ControlButtons;