import React, { Component } from 'react';
import { Editor, EditorState, RichUtils, Modifier, convertFromHTML, ContentState, getDefaultKeyBinding, KeyBindingUtil } from 'draft-js';

import ControlButtons from './ControlButtons';

import './index.css';

const {hasCommandModifier} = KeyBindingUtil;
class RichTextEditor extends Component {

  constructor (props) {
    super(props);
    // create empty editor content
    this.state = {
      editorState: EditorState.createEmpty(),
    }
    this.onChange = (editorState) => {
      
      // for(let amount of editorState.getCurrentContent().get('entityMap')) {
      //   console.log(amount); // 500 гр, 350 гр, 50 гр
      // }
      this.setState({
      editorState,
    });}
  }

  // focus on editor
  focus = () => this.refs.editor.focus();

  // this function allow us to save and display changes while user is writing
  

  // handleKeyCommand func allow us to use keybinding command, like ctrl+b => bold and so on
  /*
    The command argument supplied to handleKeyCommand is a string value ("bold"), 
    the name of the command to be executed. This is mapped from a DOM key event. 
    The editorState argument represents the latest editor state as it might be changed 
    internally by draft when handling the key. Use this instance of the editor state 
    inside handleKeyCommand.
  */
  handleKeyCommand = (command, editorState) => {
    if (command === 'myeditor-ctrlV') {
      this.appendBlocksFromHtml();
    }
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      this.onChange(newState);
      return true;
    }
    return false;
  }

  myKeyBindingFn = (e) => {
    if (e.keyCode === 83 /* `S` key */ && hasCommandModifier(e)) {
      return 'myeditor-ctrlV';
    }
    return getDefaultKeyBinding(e);
  }
    
  // handleTab allow us to use Tab key in text editor. Use this func with "onTab" prop
  handleTab = (event) => {
    const tabCharacter = "      ";
    event.preventDefault();
    
    let currentState = this.state.editorState;
    let newContentState = Modifier.replaceText(
      currentState.getCurrentContent(),
      currentState.getSelection(),
        tabCharacter
      );
    
    this.setState({ 
      editorState: EditorState.push(currentState, newContentState, 'insert-characters')
    });    
  }

  // toggleBlockType func allow to change block styles, as H1, H2 ...
  toggleBlockType = (blocktype) => {
    this.onChange(
      RichUtils.toggleBlockType(
        this.state.editorState,
        blocktype
      )
    );
  }

  // toggleInlineStyle func allow to change inline styles, as bold, italic ...
  toggleInlineStyle = (inlineStyle) => {
    this.onChange(
      RichUtils.toggleInlineStyle(
        this.state.editorState,
        inlineStyle
      )
    );
  }

  appendBlocksFromHtml = () => {
    const editorState = this.state.editorState;
    const selectionState = editorState.getSelection();
    const anchorKey = selectionState.getAnchorKey();
    const currentContent = editorState.getCurrentContent();
    const currentContentBlock = currentContent.getBlockForKey(anchorKey);
    const start = selectionState.getStartOffset();
    const end = selectionState.getEndOffset();
    // return selection text: string
    const selectedText = currentContentBlock.getText().slice(start, end);
    //create new block from string
    const newBlockMap = convertFromHTML(selectedText);
    console.log(selectedText);
    
    const newContentState =
    ContentState.createFromBlockArray(newBlockMap,newBlockMap.entityMap);
    const newEditorState = EditorState.createWithContent(newContentState);
    this.setState({editorState: newEditorState});
}
  
  render() {
    const {editorState} = this.state;
    let className = 'editor';

    // hide placeholder if any block style is applied
    const contentState = editorState.getCurrentContent();
    if (!contentState.hasText()) {
      if (contentState.getBlockMap().first().getType() !== 'unstyled') {
        className += ' hidePlaceholder';
      }
    }

    return (
      <div className = "editor-root">
        <BlockStyleControls 
          editorState = {editorState}
          onToggle = {this.toggleBlockType}
          addHtml = {this.appendBlocksFromHtml}
        />
        <InlineStyleControls 
          editorState = {editorState}
          onToggle = {this.toggleInlineStyle}
        />
        <div 
          className = {className}
          onClick = {this.focus}
        >
          <Editor
            blockStyleFn = {getBlockStyle}
            customStyleMap = {styleMap}
            editorState = {this.state.editorState}
            onChange = {this.onChange}
            handleKeyCommand = {this.handleKeyCommand}
            onTab = {this.handleTab}
            keyBindingFn={this.myKeyBindingFn}
            placeholder = 'Rich text editor'
            spellCheck = {true}
            ref = "editor"
          />
        </div>
        <pre>{this.state.editorContentHtml}</pre>
      </div>
    );
  }
}

export default RichTextEditor;

// customStyleMap prop allow us to define style objects (add Strikethrough style)
const styleMap = {
  'STRIKETHROUGH': {
    textDecoration: 'line-through',
  },
};

// the blockStyleFn prop on Editor allows you to define CSS classes to style blocks at render time.
function getBlockStyle(block) {
  switch (block.getType()) {
    case 'header-one':
      return 'my-header-one';
    
    case 'header-three':
      return 'my-header-three';
  
    default:
      return null;
  }
}

const BLOCK_TYPES = [
  {label: 'H1', style: 'header-one'},
  {label: 'H2', style: 'header-two'},
  {label: 'H3', style: 'header-three'},
  {label: 'H4', style: 'header-four'},
  {label: 'H5', style: 'header-five'},
  {label: 'H6', style: 'header-six'},
  {label: 'Blockquote', style: 'blockquote'},
  {label: 'UL', style: 'unordered-list-item'},
  {label: 'OL', style: 'ordered-list-item'},
  {label: 'Code Block', style: 'code-block'},
];

// create block style buttons
const BlockStyleControls = (props) => {
  const {editorState} = props;

  // .getSelection() - returns the current cursor/selection state of the editor
  const selection = editorState.getSelection();
  
  /*
  .getCurrentContent() - returns the current contents of the editor
  .getBlockForKey() - returns the ContentBlock corresponding to the given block key
  .getStartKey() - returns the key of the block containing the start position of the selection range
  .getType() - returns the type for this ContentBlock. Type values are largely analogous to block-level HTML elements
  */
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();

  return (
    <div className = "controls">
      {BLOCK_TYPES.map((type) => 
        <ControlButtons 
          key = {type.label}
          active = {type.style === blockType}
          label = {type.label}
          onToggle = {props.onToggle}
          style = {type.style}
        />
      )}
      <ControlButtons 
          key = {'HTML'}
          label = {'HTML'}
          onToggle = {props.addHtml}
        />
    </div>
  )
}

var INLINE_STYLES = [
  {label: 'Bold', style: 'BOLD'},
  {label: 'Italic', style: 'ITALIC'},
  {label: 'Underline', style: 'UNDERLINE'},
  {label: 'Monospace', style: 'CODE'},
  {label: 'Strikethrough', style: 'STRIKETHROUGH'}
];

const InlineStyleControls = (props) => {

  // .getCurrentInlineStyle() - returns an OrderedSet<string> that represents the "current" inline style for the editor
  const currentStyle = props.editorState.getCurrentInlineStyle();

  return (
    <div className = "controls">
      {INLINE_STYLES.map((type) => 
        <ControlButtons 
          key = {type.label}
          active = {currentStyle.has(type.style)}
          label = {type.label}
          onToggle = {props.onToggle}
          style = {type.style}
        />  
      )}
    </div>
  )
}